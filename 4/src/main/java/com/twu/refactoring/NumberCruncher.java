package com.twu.refactoring;

import java.util.Arrays;

public class NumberCruncher {
    private final int[] numbers;

    public NumberCruncher(int... numbers) {
        this.numbers = numbers;
    }

    private int count(Condition condition){
        return (int) Arrays.stream(numbers).filter(condition::judge).count();
    }

    public int countEven() {
        return count(number -> number%2==0);
    }

    public int countOdd() {
        return count(number -> number%2==1);
    }

    public int countPositive() {
        return count(number -> number>=0);
    }

    public int countNegative() {
        return count(number -> number<0);
    }
}
