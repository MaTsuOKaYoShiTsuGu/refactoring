package com.twu.refactoring;

public interface Condition {
    boolean judge(int number);
}
