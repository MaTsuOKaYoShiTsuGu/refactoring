package com.twu.refactoring;

import java.util.HashMap;
import java.util.Map;

public class Direction {
    private final char direction;

    public Direction(char direction) {
        this.direction = direction;
    }

    public Direction turnRight() {
        Map<Character, Character> turnRightMap = new HashMap<>();
        turnRightMap.put('N','E');
        turnRightMap.put('S','W');
        turnRightMap.put('E','N');
        turnRightMap.put('W','S');
        if(turnRightMap.containsKey(direction))
            return new Direction(turnRightMap.get(direction));
        throw new IllegalArgumentException();
    }

    public Direction turnLeft() {
        Map<Character, Character> turnLeftMap = new HashMap<>();
        turnLeftMap.put('N','W');
        turnLeftMap.put('S','E');
        turnLeftMap.put('E','N');
        turnLeftMap.put('W','S');
        if(turnLeftMap.containsKey(direction))
            return new Direction(turnLeftMap.get(direction));
        throw new IllegalArgumentException();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Direction direction = (Direction) object;

        return this.direction == direction.direction;
    }

    @Override
    public int hashCode() {
        return (int) direction;
    }

    @Override
    public String toString() {
        return "Direction{direction=" + direction + '}';
    }
}
