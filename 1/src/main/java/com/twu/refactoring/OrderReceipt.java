package com.twu.refactoring;

import java.util.List;

public class OrderReceipt {
    private Order order;

    private final double taxRate = 0.1;

    public OrderReceipt(Order order) {
        this.order = order;
	}

	public String printReceipt() {
		StringBuilder output = new StringBuilder();

        PrintHeaders(output);

        PrintUserInformation(output);

        PrintsLineItems(output);

        PrintsTheStateTax(output, order.getLineItems());

        PrintTotalAmount(output, order.getLineItems());

        return output.toString();
	}

    private void PrintsLineItems(StringBuilder output) {
        for (LineItem lineItem : order.getLineItems()) {
            output.append(lineItem.getDescription());
            output.append('\t');
            output.append(lineItem.getPrice());
            output.append('\t');
            output.append(lineItem.getQuantity());
            output.append('\t');
            output.append(lineItem.totalAmount());
            output.append('\n');
        }
    }

    private void PrintTotalAmount(StringBuilder output, List<LineItem> lineItems) {
        double totalAmount = lineItems.stream().mapToDouble(LineItem::totalAmount).sum()*(1+taxRate);
        output.append("Total Amount").append('\t').append(totalAmount);
    }

    private void PrintsTheStateTax(StringBuilder output, List<LineItem> lineItems) {
        double totSalesTx = lineItems.stream().mapToDouble(LineItem::totalAmount).sum()*taxRate;
        output.append("Sales Tax").append('\t').append(totSalesTx);
    }

    private void PrintUserInformation(StringBuilder output) {
        output.append(order.getCustomerName());
        output.append(order.getCustomerAddress());
    }

    private void PrintHeaders(StringBuilder output) {
        output.append("======Printing Orders======\n");
    }
}